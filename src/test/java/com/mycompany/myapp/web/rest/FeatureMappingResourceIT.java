package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.FeatureMapping;
import com.mycompany.myapp.domain.enumeration.FeatureTargetType;
import com.mycompany.myapp.repository.FeatureMappingRepository;
import com.mycompany.myapp.repository.search.FeatureMappingSearchRepository;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FeatureMappingResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class FeatureMappingResourceIT {

    private static final Boolean DEFAULT_IS_FEATURE_ENABLE = false;
    private static final Boolean UPDATED_IS_FEATURE_ENABLE = true;

    private static final FeatureTargetType DEFAULT_TARGET_TYPE = FeatureTargetType.USER;
    private static final FeatureTargetType UPDATED_TARGET_TYPE = FeatureTargetType.USER;

    private static final Long DEFAULT_TARGET_ID = 1L;
    private static final Long UPDATED_TARGET_ID = 2L;

    private static final Double DEFAULT_PRIORITY = 1D;
    private static final Double UPDATED_PRIORITY = 2D;

    private static final String ENTITY_API_URL = "/api/feature-mappings";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    private static final String ENTITY_SEARCH_API_URL = "/api/_search/feature-mappings";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FeatureMappingRepository featureMappingRepository;

    /**
     * This repository is mocked in the com.mycompany.myapp.repository.search test package.
     *
     * @see com.mycompany.myapp.repository.search.FeatureMappingSearchRepositoryMockConfiguration
     */
    @Autowired
    private FeatureMappingSearchRepository mockFeatureMappingSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFeatureMappingMockMvc;

    private FeatureMapping featureMapping;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeatureMapping createEntity(EntityManager em) {
        FeatureMapping featureMapping = new FeatureMapping()
            .isFeatureEnable(DEFAULT_IS_FEATURE_ENABLE)
            .targetType(DEFAULT_TARGET_TYPE)
            .targetId(DEFAULT_TARGET_ID)
            .priority(DEFAULT_PRIORITY);
        return featureMapping;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeatureMapping createUpdatedEntity(EntityManager em) {
        FeatureMapping featureMapping = new FeatureMapping()
            .isFeatureEnable(UPDATED_IS_FEATURE_ENABLE)
            .targetType(UPDATED_TARGET_TYPE)
            .targetId(UPDATED_TARGET_ID)
            .priority(UPDATED_PRIORITY);
        return featureMapping;
    }

    @BeforeEach
    public void initTest() {
        featureMapping = createEntity(em);
    }

    @Test
    @Transactional
    void createFeatureMapping() throws Exception {
        int databaseSizeBeforeCreate = featureMappingRepository.findAll().size();
        // Create the FeatureMapping
        restFeatureMappingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isCreated());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeCreate + 1);
        FeatureMapping testFeatureMapping = featureMappingList.get(featureMappingList.size() - 1);
        assertThat(testFeatureMapping.getIsFeatureEnable()).isEqualTo(DEFAULT_IS_FEATURE_ENABLE);
        assertThat(testFeatureMapping.getTargetType()).isEqualTo(DEFAULT_TARGET_TYPE);
        assertThat(testFeatureMapping.getTargetId()).isEqualTo(DEFAULT_TARGET_ID);
        assertThat(testFeatureMapping.getPriority()).isEqualTo(DEFAULT_PRIORITY);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(1)).save(testFeatureMapping);
    }

    @Test
    @Transactional
    void createFeatureMappingWithExistingId() throws Exception {
        // Create the FeatureMapping with an existing ID
        featureMapping.setId(1L);

        int databaseSizeBeforeCreate = featureMappingRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeatureMappingMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isBadRequest());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeCreate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void getAllFeatureMappings() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        // Get all the featureMappingList
        restFeatureMappingMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(featureMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].isFeatureEnable").value(hasItem(DEFAULT_IS_FEATURE_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].targetType").value(hasItem(DEFAULT_TARGET_TYPE.toString())))
            .andExpect(jsonPath("$.[*].targetId").value(hasItem(DEFAULT_TARGET_ID.intValue())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY.doubleValue())));
    }

    @Test
    @Transactional
    void getFeatureMapping() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        // Get the featureMapping
        restFeatureMappingMockMvc
            .perform(get(ENTITY_API_URL_ID, featureMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(featureMapping.getId().intValue()))
            .andExpect(jsonPath("$.isFeatureEnable").value(DEFAULT_IS_FEATURE_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.targetType").value(DEFAULT_TARGET_TYPE.toString()))
            .andExpect(jsonPath("$.targetId").value(DEFAULT_TARGET_ID.intValue()))
            .andExpect(jsonPath("$.priority").value(DEFAULT_PRIORITY.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingFeatureMapping() throws Exception {
        // Get the featureMapping
        restFeatureMappingMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFeatureMapping() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();

        // Update the featureMapping
        FeatureMapping updatedFeatureMapping = featureMappingRepository.findById(featureMapping.getId()).get();
        // Disconnect from session so that the updates on updatedFeatureMapping are not directly saved in db
        em.detach(updatedFeatureMapping);
        updatedFeatureMapping
            .isFeatureEnable(UPDATED_IS_FEATURE_ENABLE)
            .targetType(UPDATED_TARGET_TYPE)
            .targetId(UPDATED_TARGET_ID)
            .priority(UPDATED_PRIORITY);

        restFeatureMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedFeatureMapping.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedFeatureMapping))
            )
            .andExpect(status().isOk());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);
        FeatureMapping testFeatureMapping = featureMappingList.get(featureMappingList.size() - 1);
        assertThat(testFeatureMapping.getIsFeatureEnable()).isEqualTo(UPDATED_IS_FEATURE_ENABLE);
        assertThat(testFeatureMapping.getTargetType()).isEqualTo(UPDATED_TARGET_TYPE);
        assertThat(testFeatureMapping.getTargetId()).isEqualTo(UPDATED_TARGET_ID);
        assertThat(testFeatureMapping.getPriority()).isEqualTo(UPDATED_PRIORITY);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository).save(testFeatureMapping);
    }

    @Test
    @Transactional
    void putNonExistingFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, featureMapping.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isBadRequest());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void putWithIdMismatchFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isBadRequest());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(featureMapping)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void partialUpdateFeatureMappingWithPatch() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();

        // Update the featureMapping using partial update
        FeatureMapping partialUpdatedFeatureMapping = new FeatureMapping();
        partialUpdatedFeatureMapping.setId(featureMapping.getId());

        partialUpdatedFeatureMapping.targetId(UPDATED_TARGET_ID);

        restFeatureMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFeatureMapping.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFeatureMapping))
            )
            .andExpect(status().isOk());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);
        FeatureMapping testFeatureMapping = featureMappingList.get(featureMappingList.size() - 1);
        assertThat(testFeatureMapping.getIsFeatureEnable()).isEqualTo(DEFAULT_IS_FEATURE_ENABLE);
        assertThat(testFeatureMapping.getTargetType()).isEqualTo(DEFAULT_TARGET_TYPE);
        assertThat(testFeatureMapping.getTargetId()).isEqualTo(UPDATED_TARGET_ID);
        assertThat(testFeatureMapping.getPriority()).isEqualTo(DEFAULT_PRIORITY);
    }

    @Test
    @Transactional
    void fullUpdateFeatureMappingWithPatch() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();

        // Update the featureMapping using partial update
        FeatureMapping partialUpdatedFeatureMapping = new FeatureMapping();
        partialUpdatedFeatureMapping.setId(featureMapping.getId());

        partialUpdatedFeatureMapping
            .isFeatureEnable(UPDATED_IS_FEATURE_ENABLE)
            .targetType(UPDATED_TARGET_TYPE)
            .targetId(UPDATED_TARGET_ID)
            .priority(UPDATED_PRIORITY);

        restFeatureMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFeatureMapping.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFeatureMapping))
            )
            .andExpect(status().isOk());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);
        FeatureMapping testFeatureMapping = featureMappingList.get(featureMappingList.size() - 1);
        assertThat(testFeatureMapping.getIsFeatureEnable()).isEqualTo(UPDATED_IS_FEATURE_ENABLE);
        assertThat(testFeatureMapping.getTargetType()).isEqualTo(UPDATED_TARGET_TYPE);
        assertThat(testFeatureMapping.getTargetId()).isEqualTo(UPDATED_TARGET_ID);
        assertThat(testFeatureMapping.getPriority()).isEqualTo(UPDATED_PRIORITY);
    }

    @Test
    @Transactional
    void patchNonExistingFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, featureMapping.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isBadRequest());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isBadRequest());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFeatureMapping() throws Exception {
        int databaseSizeBeforeUpdate = featureMappingRepository.findAll().size();
        featureMapping.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFeatureMappingMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(featureMapping))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the FeatureMapping in the database
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(0)).save(featureMapping);
    }

    @Test
    @Transactional
    void deleteFeatureMapping() throws Exception {
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);

        int databaseSizeBeforeDelete = featureMappingRepository.findAll().size();

        // Delete the featureMapping
        restFeatureMappingMockMvc
            .perform(delete(ENTITY_API_URL_ID, featureMapping.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FeatureMapping> featureMappingList = featureMappingRepository.findAll();
        assertThat(featureMappingList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FeatureMapping in Elasticsearch
        verify(mockFeatureMappingSearchRepository, times(1)).deleteById(featureMapping.getId());
    }

    @Test
    @Transactional
    void searchFeatureMapping() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        featureMappingRepository.saveAndFlush(featureMapping);
        when(mockFeatureMappingSearchRepository.search("id:" + featureMapping.getId())).thenReturn(Stream.of(featureMapping));

        // Search the featureMapping
        restFeatureMappingMockMvc
            .perform(get(ENTITY_SEARCH_API_URL + "?query=id:" + featureMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(featureMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].isFeatureEnable").value(hasItem(DEFAULT_IS_FEATURE_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].targetType").value(hasItem(DEFAULT_TARGET_TYPE.toString())))
            .andExpect(jsonPath("$.[*].targetId").value(hasItem(DEFAULT_TARGET_ID.intValue())))
            .andExpect(jsonPath("$.[*].priority").value(hasItem(DEFAULT_PRIORITY.doubleValue())));
    }
}
