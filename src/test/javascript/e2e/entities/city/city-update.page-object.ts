import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class CityUpdatePage {
  pageTitle: ElementFinder = element(by.id('jhipsterMonoReactApplicationApp.city.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  cityNameInput: ElementFinder = element(by.css('input#city-cityName'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCityNameInput(cityName) {
    await this.cityNameInput.sendKeys(cityName);
  }

  async getCityNameInput() {
    return this.cityNameInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setCityNameInput('cityName');
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
