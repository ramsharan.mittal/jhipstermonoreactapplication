import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import FeatureMappingComponentsPage from './feature-mapping.page-object';
import FeatureMappingUpdatePage from './feature-mapping-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('FeatureMapping e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let featureMappingComponentsPage: FeatureMappingComponentsPage;
  let featureMappingUpdatePage: FeatureMappingUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    featureMappingComponentsPage = new FeatureMappingComponentsPage();
    featureMappingComponentsPage = await featureMappingComponentsPage.goToPage(navBarPage);
  });

  it('should load FeatureMappings', async () => {
    expect(await featureMappingComponentsPage.title.getText()).to.match(/Feature Mappings/);
    expect(await featureMappingComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete FeatureMappings', async () => {
    const beforeRecordsCount = (await isVisible(featureMappingComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(featureMappingComponentsPage.table);
    featureMappingUpdatePage = await featureMappingComponentsPage.goToCreateFeatureMapping();
    await featureMappingUpdatePage.enterData();
    expect(await isVisible(featureMappingUpdatePage.saveButton)).to.be.false;

    expect(await featureMappingComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(featureMappingComponentsPage.table);
    await waitUntilCount(featureMappingComponentsPage.records, beforeRecordsCount + 1);
    expect(await featureMappingComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await featureMappingComponentsPage.deleteFeatureMapping();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(featureMappingComponentsPage.records, beforeRecordsCount);
      expect(await featureMappingComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(featureMappingComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
