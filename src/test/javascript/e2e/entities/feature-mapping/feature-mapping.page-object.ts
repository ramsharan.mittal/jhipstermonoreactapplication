import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import FeatureMappingUpdatePage from './feature-mapping-update.page-object';

const expect = chai.expect;
export class FeatureMappingDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('jhipsterMonoReactApplicationApp.featureMapping.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-featureMapping'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class FeatureMappingComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('feature-mapping-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('feature-mapping');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateFeatureMapping() {
    await this.createButton.click();
    return new FeatureMappingUpdatePage();
  }

  async deleteFeatureMapping() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const featureMappingDeleteDialog = new FeatureMappingDeleteDialog();
    await waitUntilDisplayed(featureMappingDeleteDialog.deleteModal);
    expect(await featureMappingDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /jhipsterMonoReactApplicationApp.featureMapping.delete.question/
    );
    await featureMappingDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(featureMappingDeleteDialog.deleteModal);

    expect(await isVisible(featureMappingDeleteDialog.deleteModal)).to.be.false;
  }
}
