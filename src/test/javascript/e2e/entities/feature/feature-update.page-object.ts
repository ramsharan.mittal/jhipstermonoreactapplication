import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class FeatureUpdatePage {
  pageTitle: ElementFinder = element(by.id('jhipsterMonoReactApplicationApp.feature.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#feature-name'));
  isEnableInput: ElementFinder = element(by.css('input#feature-isEnable'));
  valueInput: ElementFinder = element(by.css('input#feature-value'));
  key1Input: ElementFinder = element(by.css('input#feature-key1'));
  value1Input: ElementFinder = element(by.css('input#feature-value1'));
  key2Input: ElementFinder = element(by.css('input#feature-key2'));
  value2Input: ElementFinder = element(by.css('input#feature-value2'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  getIsEnableInput() {
    return this.isEnableInput;
  }
  async setValueInput(value) {
    await this.valueInput.sendKeys(value);
  }

  async getValueInput() {
    return this.valueInput.getAttribute('value');
  }

  async setKey1Input(key1) {
    await this.key1Input.sendKeys(key1);
  }

  async getKey1Input() {
    return this.key1Input.getAttribute('value');
  }

  async setValue1Input(value1) {
    await this.value1Input.sendKeys(value1);
  }

  async getValue1Input() {
    return this.value1Input.getAttribute('value');
  }

  async setKey2Input(key2) {
    await this.key2Input.sendKeys(key2);
  }

  async getKey2Input() {
    return this.key2Input.getAttribute('value');
  }

  async setValue2Input(value2) {
    await this.value2Input.sendKeys(value2);
  }

  async getValue2Input() {
    return this.value2Input.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setNameInput('name');
    await waitUntilDisplayed(this.saveButton);
    const selectedIsEnable = await this.getIsEnableInput().isSelected();
    if (selectedIsEnable) {
      await this.getIsEnableInput().click();
    } else {
      await this.getIsEnableInput().click();
    }
    await waitUntilDisplayed(this.saveButton);
    await this.setValueInput('value');
    await waitUntilDisplayed(this.saveButton);
    await this.setKey1Input('key1');
    await waitUntilDisplayed(this.saveButton);
    await this.setValue1Input('value1');
    await waitUntilDisplayed(this.saveButton);
    await this.setKey2Input('key2');
    await waitUntilDisplayed(this.saveButton);
    await this.setValue2Input('value2');
    await this.save();
    await waitUntilHidden(this.saveButton);
  }
}
