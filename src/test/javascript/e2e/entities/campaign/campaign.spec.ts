import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import CampaignComponentsPage from './campaign.page-object';
import CampaignUpdatePage from './campaign-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Campaign e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let campaignComponentsPage: CampaignComponentsPage;
  let campaignUpdatePage: CampaignUpdatePage;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();
    await signInPage.username.sendKeys(username);
    await signInPage.password.sendKeys(password);
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    campaignComponentsPage = new CampaignComponentsPage();
    campaignComponentsPage = await campaignComponentsPage.goToPage(navBarPage);
  });

  it('should load Campaigns', async () => {
    expect(await campaignComponentsPage.title.getText()).to.match(/Campaigns/);
    expect(await campaignComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Campaigns', async () => {
    const beforeRecordsCount = (await isVisible(campaignComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(campaignComponentsPage.table);
    campaignUpdatePage = await campaignComponentsPage.goToCreateCampaign();
    await campaignUpdatePage.enterData();
    expect(await isVisible(campaignUpdatePage.saveButton)).to.be.false;

    expect(await campaignComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(campaignComponentsPage.table);
    await waitUntilCount(campaignComponentsPage.records, beforeRecordsCount + 1);
    expect(await campaignComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await campaignComponentsPage.deleteCampaign();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(campaignComponentsPage.records, beforeRecordsCount);
      expect(await campaignComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(campaignComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
