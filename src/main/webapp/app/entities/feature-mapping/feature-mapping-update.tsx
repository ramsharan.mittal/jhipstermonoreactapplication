import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IFeature } from 'app/shared/model/feature.model';
import { getEntities as getFeatures } from 'app/entities/feature/feature.reducer';
import { getEntity, updateEntity, createEntity, reset } from './feature-mapping.reducer';
import { IFeatureMapping } from 'app/shared/model/feature-mapping.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { FeatureTargetType } from 'app/shared/model/enumerations/feature-target-type.model';

export const FeatureMappingUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const features = useAppSelector(state => state.feature.entities);
  const featureMappingEntity = useAppSelector(state => state.featureMapping.entity);
  const loading = useAppSelector(state => state.featureMapping.loading);
  const updating = useAppSelector(state => state.featureMapping.updating);
  const updateSuccess = useAppSelector(state => state.featureMapping.updateSuccess);
  const featureTargetTypeValues = Object.keys(FeatureTargetType);
  const handleClose = () => {
    props.history.push('/feature-mapping');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getFeatures({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...featureMappingEntity,
      ...values,
      featureId: features.find(it => it.id.toString() === values.featureId.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          targetType: 'USER',
          ...featureMappingEntity,
          featureId: featureMappingEntity?.featureId?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="jhipsterMonoReactApplicationApp.featureMapping.home.createOrEditLabel" data-cy="FeatureMappingCreateUpdateHeading">
            <Translate contentKey="jhipsterMonoReactApplicationApp.featureMapping.home.createOrEditLabel">
              Create or edit a FeatureMapping
            </Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="feature-mapping-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('jhipsterMonoReactApplicationApp.featureMapping.isFeatureEnable')}
                id="feature-mapping-isFeatureEnable"
                name="isFeatureEnable"
                data-cy="isFeatureEnable"
                check
                type="checkbox"
              />
              <ValidatedField
                label={translate('jhipsterMonoReactApplicationApp.featureMapping.targetType')}
                id="feature-mapping-targetType"
                name="targetType"
                data-cy="targetType"
                type="select"
              >
                {featureTargetTypeValues.map(featureTargetType => (
                  <option value={featureTargetType} key={featureTargetType}>
                    {translate('jhipsterMonoReactApplicationApp.FeatureTargetType.' + featureTargetType)}
                  </option>
                ))}
              </ValidatedField>
              <ValidatedField
                label={translate('jhipsterMonoReactApplicationApp.featureMapping.targetId')}
                id="feature-mapping-targetId"
                name="targetId"
                data-cy="targetId"
                type="text"
              />
              <ValidatedField
                label={translate('jhipsterMonoReactApplicationApp.featureMapping.priority')}
                id="feature-mapping-priority"
                name="priority"
                data-cy="priority"
                type="text"
              />
              <ValidatedField
                id="feature-mapping-featureId"
                name="featureId"
                data-cy="featureId"
                label={translate('jhipsterMonoReactApplicationApp.featureMapping.featureId')}
                type="select"
              >
                <option value="" key="0" />
                {features
                  ? features.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/feature-mapping" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default FeatureMappingUpdate;
