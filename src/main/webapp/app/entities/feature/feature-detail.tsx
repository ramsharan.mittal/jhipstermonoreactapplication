import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './feature.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FeatureDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const featureEntity = useAppSelector(state => state.feature.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="featureDetailsHeading">
          <Translate contentKey="jhipsterMonoReactApplicationApp.feature.detail.title">Feature</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{featureEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.name">Name</Translate>
            </span>
          </dt>
          <dd>{featureEntity.name}</dd>
          <dt>
            <span id="isEnable">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.isEnable">Is Enable</Translate>
            </span>
          </dt>
          <dd>{featureEntity.isEnable ? 'true' : 'false'}</dd>
          <dt>
            <span id="value">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.value">Value</Translate>
            </span>
          </dt>
          <dd>{featureEntity.value}</dd>
          <dt>
            <span id="key1">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.key1">Key 1</Translate>
            </span>
          </dt>
          <dd>{featureEntity.key1}</dd>
          <dt>
            <span id="value1">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.value1">Value 1</Translate>
            </span>
          </dt>
          <dd>{featureEntity.value1}</dd>
          <dt>
            <span id="key2">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.key2">Key 2</Translate>
            </span>
          </dt>
          <dd>{featureEntity.key2}</dd>
          <dt>
            <span id="value2">
              <Translate contentKey="jhipsterMonoReactApplicationApp.feature.value2">Value 2</Translate>
            </span>
          </dt>
          <dd>{featureEntity.value2}</dd>
        </dl>
        <Button tag={Link} to="/feature" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/feature/${featureEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default FeatureDetail;
