export interface ICampaign {
  id?: number;
  campaignName?: string | null;
  campaignUrl?: string | null;
  campaignType?: string | null;
}

export const defaultValue: Readonly<ICampaign> = {};
