import { IFeature } from 'app/shared/model/feature.model';
import { FeatureTargetType } from 'app/shared/model/enumerations/feature-target-type.model';

export interface IFeatureMapping {
  id?: number;
  isFeatureEnable?: boolean | null;
  targetType?: FeatureTargetType | null;
  targetId?: number | null;
  priority?: number | null;
  featureId?: IFeature | null;
}

export const defaultValue: Readonly<IFeatureMapping> = {
  isFeatureEnable: false,
};
