export interface IProject {
  id?: number;
  projectName?: string | null;
  projectUrl?: string | null;
}

export const defaultValue: Readonly<IProject> = {};
