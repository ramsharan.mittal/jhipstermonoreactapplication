export interface ICity {
  id?: number;
  cityName?: string | null;
}

export const defaultValue: Readonly<ICity> = {};
