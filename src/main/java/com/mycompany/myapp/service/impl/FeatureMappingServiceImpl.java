package com.mycompany.myapp.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mycompany.myapp.domain.FeatureMapping;
import com.mycompany.myapp.repository.FeatureMappingRepository;
import com.mycompany.myapp.repository.search.FeatureMappingSearchRepository;
import com.mycompany.myapp.service.FeatureMappingService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FeatureMapping}.
 */
@Service
@Transactional
public class FeatureMappingServiceImpl implements FeatureMappingService {

    private final Logger log = LoggerFactory.getLogger(FeatureMappingServiceImpl.class);

    private final FeatureMappingRepository featureMappingRepository;

    private final FeatureMappingSearchRepository featureMappingSearchRepository;

    public FeatureMappingServiceImpl(
        FeatureMappingRepository featureMappingRepository,
        FeatureMappingSearchRepository featureMappingSearchRepository
    ) {
        this.featureMappingRepository = featureMappingRepository;
        this.featureMappingSearchRepository = featureMappingSearchRepository;
    }

    @Override
    public FeatureMapping save(FeatureMapping featureMapping) {
        log.debug("Request to save FeatureMapping : {}", featureMapping);
        FeatureMapping result = featureMappingRepository.save(featureMapping);
        featureMappingSearchRepository.save(result);
        return result;
    }

    @Override
    public Optional<FeatureMapping> partialUpdate(FeatureMapping featureMapping) {
        log.debug("Request to partially update FeatureMapping : {}", featureMapping);

        return featureMappingRepository
            .findById(featureMapping.getId())
            .map(existingFeatureMapping -> {
                if (featureMapping.getIsFeatureEnable() != null) {
                    existingFeatureMapping.setIsFeatureEnable(featureMapping.getIsFeatureEnable());
                }
                if (featureMapping.getTargetType() != null) {
                    existingFeatureMapping.setTargetType(featureMapping.getTargetType());
                }
                if (featureMapping.getTargetId() != null) {
                    existingFeatureMapping.setTargetId(featureMapping.getTargetId());
                }
                if (featureMapping.getPriority() != null) {
                    existingFeatureMapping.setPriority(featureMapping.getPriority());
                }

                return existingFeatureMapping;
            })
            .map(featureMappingRepository::save)
            .map(savedFeatureMapping -> {
                featureMappingSearchRepository.save(savedFeatureMapping);

                return savedFeatureMapping;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public List<FeatureMapping> findAll() {
        log.debug("Request to get all FeatureMappings");
        return featureMappingRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FeatureMapping> findOne(Long id) {
        log.debug("Request to get FeatureMapping : {}", id);
        return featureMappingRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete FeatureMapping : {}", id);
        featureMappingRepository.deleteById(id);
        featureMappingSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FeatureMapping> search(String query) {
        log.debug("Request to search FeatureMappings for query {}", query);
        return StreamSupport.stream(featureMappingSearchRepository.search(query).spliterator(), false).collect(Collectors.toList());
    }
}
