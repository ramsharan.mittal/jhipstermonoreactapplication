package com.mycompany.myapp.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.*;

import com.mycompany.myapp.domain.Campaign;
import com.mycompany.myapp.repository.CampaignRepository;
import com.mycompany.myapp.repository.search.CampaignSearchRepository;
import com.mycompany.myapp.service.CampaignService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Campaign}.
 */
@Service
@Transactional
public class CampaignServiceImpl implements CampaignService {

    private final Logger log = LoggerFactory.getLogger(CampaignServiceImpl.class);

    private final CampaignRepository campaignRepository;

    private final CampaignSearchRepository campaignSearchRepository;

    public CampaignServiceImpl(CampaignRepository campaignRepository, CampaignSearchRepository campaignSearchRepository) {
        this.campaignRepository = campaignRepository;
        this.campaignSearchRepository = campaignSearchRepository;
    }

    @Override
    public Campaign save(Campaign campaign) {
        log.debug("Request to save Campaign : {}", campaign);
        Campaign result = campaignRepository.save(campaign);
        campaignSearchRepository.save(result);
        return result;
    }

    @Override
    public Optional<Campaign> partialUpdate(Campaign campaign) {
        log.debug("Request to partially update Campaign : {}", campaign);

        return campaignRepository
            .findById(campaign.getId())
            .map(existingCampaign -> {
                if (campaign.getCampaignName() != null) {
                    existingCampaign.setCampaignName(campaign.getCampaignName());
                }
                if (campaign.getCampaignUrl() != null) {
                    existingCampaign.setCampaignUrl(campaign.getCampaignUrl());
                }
                if (campaign.getCampaignType() != null) {
                    existingCampaign.setCampaignType(campaign.getCampaignType());
                }

                return existingCampaign;
            })
            .map(campaignRepository::save)
            .map(savedCampaign -> {
                campaignSearchRepository.save(savedCampaign);

                return savedCampaign;
            });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Campaign> findAll(Pageable pageable) {
        log.debug("Request to get all Campaigns");
        return campaignRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Campaign> findOne(Long id) {
        log.debug("Request to get Campaign : {}", id);
        return campaignRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Campaign : {}", id);
        campaignRepository.deleteById(id);
        campaignSearchRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Campaign> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Campaigns for query {}", query);
        return campaignSearchRepository.search(query, pageable);
    }
}
