package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.FeatureMapping;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link FeatureMapping}.
 */
public interface FeatureMappingService {
    /**
     * Save a featureMapping.
     *
     * @param featureMapping the entity to save.
     * @return the persisted entity.
     */
    FeatureMapping save(FeatureMapping featureMapping);

    /**
     * Partially updates a featureMapping.
     *
     * @param featureMapping the entity to update partially.
     * @return the persisted entity.
     */
    Optional<FeatureMapping> partialUpdate(FeatureMapping featureMapping);

    /**
     * Get all the featureMappings.
     *
     * @return the list of entities.
     */
    List<FeatureMapping> findAll();

    /**
     * Get the "id" featureMapping.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FeatureMapping> findOne(Long id);

    /**
     * Delete the "id" featureMapping.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Search for the featureMapping corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    List<FeatureMapping> search(String query);
}
