package com.mycompany.myapp.domain;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Feature.
 */
@Entity
@Table(name = "feature")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "feature")
public class Feature implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "is_enable")
    private Boolean isEnable;

    @Column(name = "value")
    private String value;

    @Column(name = "key_1")
    private String key1;

    @Column(name = "value_1")
    private String value1;

    @Column(name = "key_2")
    private String key2;

    @Column(name = "value_2")
    private String value2;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Feature id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Feature name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsEnable() {
        return this.isEnable;
    }

    public Feature isEnable(Boolean isEnable) {
        this.setIsEnable(isEnable);
        return this;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public String getValue() {
        return this.value;
    }

    public Feature value(String value) {
        this.setValue(value);
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey1() {
        return this.key1;
    }

    public Feature key1(String key1) {
        this.setKey1(key1);
        return this;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getValue1() {
        return this.value1;
    }

    public Feature value1(String value1) {
        this.setValue1(value1);
        return this;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getKey2() {
        return this.key2;
    }

    public Feature key2(String key2) {
        this.setKey2(key2);
        return this;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getValue2() {
        return this.value2;
    }

    public Feature value2(String value2) {
        this.setValue2(value2);
        return this;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Feature)) {
            return false;
        }
        return id != null && id.equals(((Feature) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Feature{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", isEnable='" + getIsEnable() + "'" +
            ", value='" + getValue() + "'" +
            ", key1='" + getKey1() + "'" +
            ", value1='" + getValue1() + "'" +
            ", key2='" + getKey2() + "'" +
            ", value2='" + getValue2() + "'" +
            "}";
    }
}
