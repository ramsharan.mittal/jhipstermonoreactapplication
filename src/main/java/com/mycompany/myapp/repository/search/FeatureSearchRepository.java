package com.mycompany.myapp.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import com.mycompany.myapp.domain.Feature;
import java.util.stream.Stream;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Feature} entity.
 */
public interface FeatureSearchRepository extends ElasticsearchRepository<Feature, Long>, FeatureSearchRepositoryInternal {}

interface FeatureSearchRepositoryInternal {
    Stream<Feature> search(String query);
}

class FeatureSearchRepositoryInternalImpl implements FeatureSearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    FeatureSearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Stream<Feature> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return elasticsearchTemplate.search(nativeSearchQuery, Feature.class).map(SearchHit::getContent).stream();
    }
}
