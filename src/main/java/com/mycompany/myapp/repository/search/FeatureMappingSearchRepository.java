package com.mycompany.myapp.repository.search;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import com.mycompany.myapp.domain.FeatureMapping;
import java.util.stream.Stream;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link FeatureMapping} entity.
 */
public interface FeatureMappingSearchRepository
    extends ElasticsearchRepository<FeatureMapping, Long>, FeatureMappingSearchRepositoryInternal {}

interface FeatureMappingSearchRepositoryInternal {
    Stream<FeatureMapping> search(String query);
}

class FeatureMappingSearchRepositoryInternalImpl implements FeatureMappingSearchRepositoryInternal {

    private final ElasticsearchRestTemplate elasticsearchTemplate;

    FeatureMappingSearchRepositoryInternalImpl(ElasticsearchRestTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public Stream<FeatureMapping> search(String query) {
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(queryStringQuery(query));
        return elasticsearchTemplate.search(nativeSearchQuery, FeatureMapping.class).map(SearchHit::getContent).stream();
    }
}
